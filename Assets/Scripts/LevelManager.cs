using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 2e comportement de Game Manager
public class LevelManager : MonoBehaviour
{
    public GameObject mainCamera;
    public GameObject greenPF;
    public GameObject bluePF;
    public GameObject brownPF;
    public GameObject monsterFly;
    public GameObject monsterFixe;
    public GameObject monsterDown;
    public GameObject hole;
    public GameObject springShoes;
    public GameObject hat;
    public GameObject jetpack;
    public GameObject player;
    public GameObject border;
    public GameObject startScene;

    private List<GameObject> listPF = new List<GameObject>();
    private List<GameObject> listTrap = new List<GameObject>();
    private List<GameObject> listBonus = new List<GameObject>();
    private List<GameObject> listBorder = new List<GameObject>();
    private float lastYPositionMainCam;
    private float sizeBlockCreation;
    private string lastGOSpawn;
    private GameObject playerInstance;
    private int scorePlayer;

    //Variables difficult�
    private float distanceBetweenSpawnGOMin;
    private float distanceBetweenSpawnGOMax;
    private List<float> vectorSpawnGO = new List<float>(); //<PF verte, PF marron, PF bleu, monstre, trou noire>
    private List<float> vectorSpawnBonus = new List<float>(); // <Rien, ressort, chapeau, fus�e>
    private List<float> vectorSpawnMonster = new List<float>(); // <fly, fixe, down>

    // Start is called before the first frame supdate
    void Start()
    {
        scorePlayer = GameObject.Find("Main Camera").GetComponent<CameraUp>().score;
        lastYPositionMainCam = mainCamera.transform.position.y;
        sizeBlockCreation = 2.5f;
        distanceBetweenSpawnGOMin = 0.5f;
        distanceBetweenSpawnGOMax = 1.0f;

        //Valeur VecteurSpawnGO la valeur totale doit �tre �gale � 1
        vectorSpawnGO.Add(1.0f); //PF verte
        vectorSpawnGO.Add(0.0f); //PF marron
        vectorSpawnGO.Add(0.0f); //PF bleu
        vectorSpawnGO.Add(0.0f); //monstre
        vectorSpawnGO.Add(0.0f); //trou noir

        //Valeur VecteurSpawnBonus la valeur totale doit �tre �gale � 1
        vectorSpawnBonus.Add(1.0f); //rien
        vectorSpawnBonus.Add(0.0f); //ressort
        vectorSpawnBonus.Add(0.0f); //chapeau
        vectorSpawnBonus.Add(0.0f); //fus�e

        // Vaaleur VecteurSpawnMonster la valeur totale ddoit �tre �gale � 1
        vectorSpawnMonster.Add(0.0f); //fly
        vectorSpawnMonster.Add(1.0f); //fixe
        vectorSpawnMonster.Add(0.0f); //down


        Begin();

        //Pour le moment jusqu'� la cr�ation d'une fonction de cr�ation de d�part de niveau
        Vector3 spawnPos = new Vector3(0, 0, 0);
        GameObject pf = Instantiate(hole, spawnPos, Quaternion.identity);
        pf.SetActive(false);
        listTrap.Add(pf);
        lastGOSpawn = "PF";
    }

    // Update is called once per frame
    void Update()
    {
        //Ajuste le niveau de difficult� selon le nombre de points du player
        UpgradeDifficulty();

        //Si la camera a chang� de position de plus 5 unit� de Y
        if (lastYPositionMainCam + sizeBlockCreation <= mainCamera.transform.position.y)
        {
            SpawnPlatform();
            //MAJ derni�re position camera 
            lastYPositionMainCam = mainCamera.transform.position.y;
        }

        if (listBorder[listBorder.Count - 1].transform.position.y - 10.0f <= mainCamera.transform.position.y)
        {
            SpawnBorder();
        }

        //D�truit les objets non visibles
        DestroyGO();

    }

    // Retourne la position de la derni�re plateforme pour �viter les chevauchements
    int lastPositionForPF()
    {
        int index = listPF.Count;
        for (int i = 0; i < listPF.Count; i++)
        {
            if (listPF[i].CompareTag("GreenPF") || listPF[i].CompareTag("BluePF"))
            {
                index = i;
            }
        }
        return index;
    }
    
    //Cr�ation des plateformes al�atoires
    void SpawnPlatform()
    {
        //Les �l�ments sont cr��s par block (actuelement de 2.5f)
        //Leurs positions sont obtenues par rapport au dernier objet cr��

        //Cr�er GameObject tant que le dernier �l�ment cr�� ne sort pas du block de cr�ation
        while(listPF[listPF.Count - 1].transform.position.y < mainCamera.transform.position.y + (3 * sizeBlockCreation))
        {
            //tester ici avec vecteur de proba quel va �tre l'�l�ment qui va spawner
            float eleProba = Random.Range(0.01f, 1f);
            int eleNB = 0;
            float tot = 0.0f;
            for (; eleNB < vectorSpawnGO.Count; eleNB++)
            {
                tot = tot + vectorSpawnGO[eleNB];
                if (tot > eleProba)
                {
                    break;
                }
            }

            //Optimiser la cr�ation des plateformes solides selon la derni�re plateforme enregistr�e dans la liste afin d'�viter un blocage au niveau de la hauteur
            if (eleNB == 0)// spawn PF verte
            {
                float positionX = Random.Range(-2.6f, 2.6f);
                float positionY = Random.Range(listPF[lastPositionForPF()].transform.position.y + distanceBetweenSpawnGOMin, listPF[lastPositionForPF()].transform.position.y + distanceBetweenSpawnGOMax);
                Vector3 spawnPos = new Vector3(positionX, positionY, 0);
                //V�rifie si la plateforme ne spawn pas trop proche des derniers monstres/pi�ges
                spawnPos = detectCollisionWithGO(listTrap, spawnPos);
                spawnPos = detectionCollisionWithTrap(listTrap, spawnPos);
                GameObject pf = Instantiate(greenPF, spawnPos, Quaternion.identity);
                listPF.Add(pf);

                //spawn Bonus
                spawnBonus(spawnPos);

                lastGOSpawn = "GreenPF";
            }
            else if(eleNB == 1)// spawn PF marron
            {
                float positionX = Random.Range(-2.6f, 2.6f);
                float positionY = Random.Range(listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMin, listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMax);
                Vector3 spawnPos = new Vector3(positionX, positionY, 0);
                //V�rifie si la plateforme ne spawn pas trop proche des derniers monstres/pi�ges
                spawnPos = detectCollisionWithGO(listTrap, spawnPos);
                GameObject pf = Instantiate(brownPF, spawnPos, Quaternion.identity);
                listTrap.Add(pf);

                lastGOSpawn = "BrownPF";
            }
            else if (eleNB == 2)// spawn PF bleu
            {
                float positionX = Random.Range(-2.6f, 2.6f);
                float positionY = Random.Range(listPF[lastPositionForPF()].transform.position.y + distanceBetweenSpawnGOMin, listPF[lastPositionForPF()].transform.position.y + distanceBetweenSpawnGOMax);
                Vector3 spawnPos = new Vector3(positionX, positionY, 0);
                //V�rifie si la plateforme ne spawn pas trop proche des derniers monstres/pi�ges
                spawnPos = detectCollisionWithGO(listTrap, spawnPos);
                GameObject pf = Instantiate(bluePF, spawnPos, Quaternion.identity);
                listPF.Add(pf);

                lastGOSpawn = "BleuPF";
            }
            else if (eleNB == 3)// spawn monster
            {
                if ((lastGOSpawn != "Hole" || lastGOSpawn != "Monster" || lastGOSpawn != "BrownPF") && !playerInstance.GetComponent<ControllerPlayer>().bonusJump)
                {
                    spawnMonster();
                }

            }
            else if (eleNB == 4)// spawn hole
            {
                //On v�rifie qu'un hole ne vient pas d'�tre cr�� afin d'�viter dans refaire un autre
                if((lastGOSpawn != "Hole" || lastGOSpawn != "Monster" || lastGOSpawn != "BrownPF") && !playerInstance.GetComponent<ControllerPlayer>().bonusJump)
                {
                    float positionX = Random.Range(-2.4f, 2.4f);
                    float positionY = Random.Range(listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMin, listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMax);
                    Vector3 spawnPos = new Vector3(positionX, positionY, 0);
                    //V�rifie si le troue noir ne spawn pas trop proche des derni�res platformes
                    spawnPos = detectCollisionWithGO(listPF, spawnPos);
                    GameObject pf = Instantiate(hole, spawnPos, Quaternion.identity);
                    listTrap.Add(pf);
                    lastGOSpawn = "Hole";
                }
            }
        }
    }

    void spawnMonster()
    {
        float positionX = Random.Range(-2.4f, 2.4f);
        float positionY = Random.Range(listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMin, listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMax);
        Vector3 spawnPos = new Vector3(positionX, positionY, 0);
        //V�rifie si le trou noir ne spawn pas trop proche des derni�res plateformes
        spawnPos = detectCollisionWithGO(listPF, spawnPos);
        spawnPos = detectionCollisionWithTrap(listTrap, spawnPos);

        float eleProbaMonster = Random.Range(0.01f, 1f);
        int eleNB = 0;
        float tot = 0.0f;
        for (; eleNB < vectorSpawnMonster.Count; eleNB++)
        {
            tot = tot + vectorSpawnMonster[eleNB];
            if (tot > eleProbaMonster)
            {
                break;
            }
        }
        GameObject pf;
        if (eleNB == 0)
        {
            pf = Instantiate(monsterFly, spawnPos, Quaternion.identity);
        }
        else if(eleNB == 1)
        {
            spawnPos = detectCollisionWithGO(listPF, spawnPos);
            pf = Instantiate(monsterFixe, spawnPos, Quaternion.identity);
        }
        else
        {
            pf = Instantiate(monsterDown, spawnPos, Quaternion.identity);
        }

        listTrap.Add(pf);
        lastGOSpawn = "Monster";
    }

    private Vector3 detectionCollisionWithTrap(List<GameObject> list, Vector3 spawnPos)
    {
        for (int i = 0; i < list.Count; i++)
        {
            try {
            if (1.0f > Mathf.Abs(list[i].transform.position.x - spawnPos.x) && spawnPos.y - list[i].transform.position.y < 1.3f)
            {
                //Si trap, alors faire appara�tre �l�ment entre trap et limite max
                    if(listPF[listPF.Count -1].transform.position.y + 3.0f > list[i].transform.position.y + 1.3f)
                    {
                        float positionY = Random.Range(list[i].transform.position.y + 1.3f, listPF[listPF.Count - 1].transform.position.y + 3.0f);
                        spawnPos = new Vector3(spawnPos.x, positionY, spawnPos.z);
                    }
                    else//sinon supprimer trap et renvoyer valeur spawn actuel
                    {
                        Destroy(list[i]);
                        return spawnPos;
                    }
                    //sinon supprimer trap et renvoyer valeur spawn actuel
            }
            }
            catch
            {

            }
        }
        return spawnPos;
    }

    private Vector3 detectCollisionWithGO(List <GameObject> list, Vector3 spawnPos)
    {
        for (int i = 0; i < list.Count; i++)
        {
            try
            {
                if (1.0f > Mathf.Abs(list[i].transform.position.x - spawnPos.x) && spawnPos.y - list[i].transform.position.y < 1.3f)
                {
                    if (spawnPos.x < 0)
                    {
                        float proba = Random.Range(1.15f, 2.6f);
                        spawnPos = new Vector3(proba, spawnPos.y, spawnPos.z);
                    }
                    else
                    {
                        float proba = Random.Range(-2.6f, -1.15f);
                        spawnPos = new Vector3(proba, spawnPos.y, spawnPos.z);
                    }
                }
            }
            catch
            {
                
            }
        }
        return spawnPos;
    }

    private void SpawnBorder()
    {
        Vector3 spawnPos = new Vector3(0, listBorder[listBorder.Count - 1].transform.position.y + 11.7f, 0);
        GameObject bor = Instantiate(border, spawnPos, Quaternion.identity);
        listBorder.Add(bor);
        spawnPos = new Vector3(0, listBorder[listBorder.Count - 2].transform.position.y+ 11.7f, 0);
        bor = Instantiate(border, spawnPos, new Quaternion(0, 0, 180.0f, 0));
        listBorder.Add(bor);
    }

    private void spawnBonus(Vector3 spawnPos)
    {
        float eleProbaBonus = Random.Range(0.01f, 1f);
        int eleNBBonus = 0;
        float totBonus = 0.0f;
        for (; eleNBBonus < vectorSpawnBonus.Count; eleNBBonus++)
        {
;           totBonus = totBonus + vectorSpawnBonus[eleNBBonus];
            if (totBonus > eleProbaBonus)
            {
                break;
            }
        }
        if (eleNBBonus == 1)// spawn ressort
        {
            spawnPos = new Vector3(spawnPos.x, spawnPos.y + 0.3f, spawnPos.z);
            GameObject pf = Instantiate(springShoes, spawnPos, Quaternion.identity);
            listBonus.Add(pf);
        }
        if (eleNBBonus == 2)// spawn chapeau
        {
            spawnPos = new Vector3(spawnPos.x, spawnPos.y + 0.3f, spawnPos.z);
            GameObject pf = Instantiate(hat, spawnPos, Quaternion.identity);
            listBonus.Add(pf);
        }
        if (eleNBBonus == 3)// spawn fus�e
        {
            spawnPos = new Vector3(spawnPos.x, spawnPos.y + 0.5f, spawnPos.z);
            GameObject pf = Instantiate(jetpack, spawnPos, Quaternion.identity);
            listBonus.Add(pf);
        }
    }

    private void UpgradeDifficulty()
    {
        scorePlayer = GameObject.Find("Main Camera").GetComponent<CameraUp>().score;
        //A partir de 100 points apparition des bonus et �cartement probable des PF
        //Apparition des PF marrons
        if (scorePlayer >= 100 && scorePlayer < 300)
        {
            distanceBetweenSpawnGOMin = 0.5f;
            distanceBetweenSpawnGOMax = 2.0f;

            vectorSpawnGO[0] = 0.90f ; //PF verte
            vectorSpawnGO[1] = 0.10f; //PF marron

            vectorSpawnBonus[0] = 0.77f; //rien
            vectorSpawnBonus[1] = 0.10f; //ressort
            vectorSpawnBonus[2] = 0.07f; //chapeau
            vectorSpawnBonus[3] = 0.06f; //fus�e
        }
        else if(scorePlayer >= 300 && scorePlayer < 500) // ajout plateformes bleues
        {
            vectorSpawnGO[0] = 0.85f; //PF verte
            vectorSpawnGO[1] = 0.10f; //PF marron
            vectorSpawnGO[2] = 0.05f; //PF bleu
        }
        else if (scorePlayer >= 500 && scorePlayer < 1000) //Ecartement des spawn de plateformes au max + ajout monstre fixe
        {
            distanceBetweenSpawnGOMax = 3.0f;

            vectorSpawnGO[0] = 0.80f; //PF verte
            vectorSpawnGO[1] = 0.10f; //PF marron
            vectorSpawnGO[2] = 0.05f; //PF bleu
            vectorSpawnGO[3] = 0.05f; //Monstre
        }
        else if(scorePlayer >= 1000 && scorePlayer < 1500)//ajout autre monstre + r�duction distance spawn minim
        {
            distanceBetweenSpawnGOMin = 1.0f;

            vectorSpawnMonster[0] = 0.25f; //fly
            vectorSpawnMonster[1] = 0.5f; //fixe
            vectorSpawnMonster[2] = 0.25f; //down
        }
        else if(scorePlayer >= 1500 && scorePlayer < 2000)//ajout trou noir + augmentation pop monstre
        {
            vectorSpawnGO[0] = 0.75f; //PF verte
            vectorSpawnGO[1] = 0.10f; //PF marron
            vectorSpawnGO[2] = 0.05f; //PF bleu
            vectorSpawnGO[3] = 0.07f; //Monstre
            vectorSpawnGO[4] = 0.03f; //Trou noir
        }
        else if(scorePlayer >= 2000 && scorePlayer < 2500)//r�duction distance minim de pop et r�duction des bonus
        {
            distanceBetweenSpawnGOMin = 1.5f;

            vectorSpawnBonus[0] = 0.82f; //rien
            vectorSpawnBonus[1] = 0.09f; //ressort
            vectorSpawnBonus[2] = 0.05f; //chapeau
            vectorSpawnBonus[3] = 0.04f; //fus�e
        }
        else if(scorePlayer >= 2500)// r�duction des bonus + augmentation des pi�ges et des monstres
        {
            vectorSpawnGO[0] = 0.62f; //PF verte
            vectorSpawnGO[1] = 0.15f; //PF marron
            vectorSpawnGO[2] = 0.10f; //PF bleu
            vectorSpawnGO[3] = 0.09f; //Monstre
            vectorSpawnGO[4] = 0.04f; //Trou noir

            vectorSpawnBonus[0] = 0.93f; //rien
            vectorSpawnBonus[1] = 0.4f; //ressort
            vectorSpawnBonus[2] = 0.2f; //chapeau
            vectorSpawnBonus[3] = 0.1f; //fus�e
        }
    }


    // Destruction des GO hors champs d�j� pass�s
    private void DestroyGO()
    {
        //Liste des plateformes
        for (int i = 0; i < listPF.Count; i++)
        {
            try
            {
                if (listPF[i].transform.position.y <= mainCamera.transform.position.y - 5.15f)
                {
                    listPF[i].SetActive(false);
                    Destroy(listPF[i]);
                    listPF.Remove(listPF[i]);
                    i--;
                }
            }
            catch
            {

            }
        }
        //Liste des pi�ges et monstres
        for (int i = 0; i < listTrap.Count; i++)
        {
            try {
            if (listTrap[i].transform.position.y <= mainCamera.transform.position.y - 5.6f)
            {
                listTrap[i].SetActive(false);
                Destroy(listTrap[i]);
                listTrap.Remove(listTrap[i]);
                i--;
            }
            }
            catch
            {

            }
        }
        //Liste des Bonus
        for (int i = 0; i < listBonus.Count; i++)
        {
            try {
            if (listBonus[i].transform.position.y <= mainCamera.transform.position.y - 5.15f)
            {
                listBonus[i].SetActive(false);
                Destroy(listBonus[i]);
                listBonus.Remove(listBonus[i]);
                i--;
            }
            }
            catch
            {

            }
        }
        //Liste des Bordures
        for (int i = 0; i < listBorder.Count; i++)
        {
            if (listBorder[i].transform.position.y <= mainCamera.transform.position.y - 16.0f)
            {
                listBorder[i].SetActive(false);
                Destroy(listBorder[i]);
                listBorder.Remove(listBorder[i]);
                i--;
            }
        }
    }

    //Cr�er le niveau lorsqu'on voit le menu
    void Begin()
    {
        //Spawn scene
        Vector3 spawnPos = new Vector3(-2.347517f, -3.853842f, 0);
        GameObject pf = Instantiate(startScene, spawnPos, Quaternion.identity);
        listPF.Add(pf);
        lastGOSpawn = "Scene";

        //Spawn doodle down
        spawnPos = new Vector3(-1.7f, 6.5f, 0);
        pf = Instantiate(player, spawnPos, Quaternion.identity);
        pf.GetComponent<Rigidbody2D>().freezeRotation = false;
        pf.GetComponent<Rigidbody2D>().angularVelocity = 360;
        pf.GetComponent<BoxCollider2D>().enabled = false;
        pf.GetComponent<SpriteRenderer>().sortingOrder = -4;
        listPF.Add(pf);
        lastGOSpawn = "Doodle";

        //Spawn la plateforme de d�part
        spawnPos = new Vector3(0, -5.0f, 0);
        pf = Instantiate(greenPF, spawnPos, Quaternion.identity);
        pf.transform.localScale = new Vector3(15.5f, pf.transform.localScale.y, pf.transform.localScale.z);
        pf.GetComponent<Jump>().jumpForce = 8.0f;
        listPF.Add(pf);
        lastGOSpawn = "PF";

        //spawn joueur
        spawnPos = new Vector3(-2.0f, -4.0f, 0);
        playerInstance = Instantiate(player, spawnPos, Quaternion.identity);
        //Passe le nouveau doodle � l'objet game manager
        GetComponent<GameManager>().player = playerInstance;
        //Recoller le player � la camera
        mainCamera.GetComponent<CameraUp>().doodle = playerInstance;

        //spawn monstre
        spawnPos = new Vector3(-1.8f, 2.5f, 0);
        pf = Instantiate(monsterFly, spawnPos, Quaternion.identity);
        listTrap.Add(pf);

        //Cr�ation de la bordure
        spawnPos = new Vector3(0, 0, 0);
        //V�rifie si le trou noir ne spawn pas trop proche des derni�res plateformes
        pf = Instantiate(border, spawnPos, Quaternion.identity);
        listBorder.Add(pf);
        spawnPos = new Vector3(0, 0, 0);
        pf = Instantiate(border, spawnPos, new Quaternion(0, 0, 180.0f, 1));
        listBorder.Add(pf);
        spawnPos = new Vector3(0, 11.7f, 0);
        pf = Instantiate(border, spawnPos, Quaternion.identity);
        listBorder.Add(pf);
        spawnPos = new Vector3(0, 11.7f, 0);
        pf = Instantiate(border, spawnPos, new Quaternion(0, 0, 180.0f, 1));
        listBorder.Add(pf);
    }

        public void StartGame()
    {
        //Cacher monstre du menu
        listTrap[0].SetActive(false);

        //La premiere plateforme doit �tre � une hauteur basse pour que le doodle puisse sauter dessus
        float positionX = Random.Range(-2.6f, 2.6f);
        float positionY = Random.Range(listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMin, listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMax);
        Vector3 spawnPos = new Vector3(positionX, listPF[listPF.Count - 1].transform.position.y + 0.5f, 0);
        GameObject pf = Instantiate(greenPF, spawnPos, Quaternion.identity);
        listPF.Add(pf);
        lastGOSpawn = "PF";

        //MAJ des vecteurs pour faire apparaitre que des plateformes jusqu'au y=7.0f
        while (listPF[listPF.Count - 1].transform.position.y < 7.0f)
        {
            positionX = Random.Range(-2.6f, 2.6f);
            positionY = Random.Range(listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMin, listPF[listPF.Count - 1].transform.position.y + distanceBetweenSpawnGOMax);
            spawnPos = new Vector3(positionX, positionY, 0);
            pf = Instantiate(greenPF, spawnPos, Quaternion.identity);
            listPF.Add(pf);
            lastGOSpawn = "PF";
        }
    }

    public void Reload()
    {
        //On vide toute les listes
        listPF = new List<GameObject>();
        listTrap = new List<GameObject>();
        listBonus = new List<GameObject>();
        listBorder = new List<GameObject>();

        //On detruit le doodle en cours
        Destroy(GameObject.Find("Player"));

        //On recharge le morceau de niveau du menu
        Begin();
    }
}
