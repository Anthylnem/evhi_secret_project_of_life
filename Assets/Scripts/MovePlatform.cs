using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement du prefab BluePF, se d�place sur x
public class MovePlatform : MonoBehaviour
{
    public float speedTranslate = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(speedTranslate, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= 2.6f)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speedTranslate, 0.0f);
        }
        else if (transform.position.x <= -2.6f)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speedTranslate, 0.0f);
        }
    }
}
