using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement du prefab Hole
public class HoleObject : MonoBehaviour
{
    public AudioClip HoleSound;
    public float SpeedRotation = 720;

    private AudioSource HoleAudio;
    private bool isTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        HoleAudio = GetComponent<AudioSource>();
    }

    //Si le joueur entre en collision avec le trou noir, il s'arr�te et se fait aspirer
    //fin du jeu
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && collision.GetComponent<Rigidbody2D>().velocity.y < 10)
        {
            Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
            rb.transform.position = new Vector3(transform.position.x, transform.position.y, rb.transform.position.z);
            Vector2 velocity = new Vector2(0.0f, 0.0f);
            rb.velocity = velocity;
            if (isTriggered == false)
            {
                HoleAudio.PlayOneShot(HoleSound, 1.0f);
                isTriggered = true;
            }
            rb.freezeRotation = false;
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.angularVelocity = SpeedRotation;

            GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver();
        }
    }
}
