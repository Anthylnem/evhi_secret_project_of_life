using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement des plateformes marrons
public class Fall : MonoBehaviour
{
    public AudioClip CrackSound;
    public Sprite FallSprite;
    private AudioSource CrackAudio;

    // Start is called before the first frame update
    void Start()
    {
        CrackAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //D�tecte si doodle entre en collision avec la plateforme, puis se casse
        if (collision.gameObject.CompareTag("Player") && collision.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            Debug.Log("Brown PF");
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            CrackAudio.PlayOneShot(CrackSound,1.0f);
            GetComponent<SpriteRenderer>().sprite = FallSprite;

        }
    }
}
