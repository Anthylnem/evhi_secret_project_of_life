using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement des prefabs : Monster Fixe, fly et Fall
public class MoveMonster : MonoBehaviour
{
    public float speedRotation = 0.5f;
    public float speedTranslate = 2.0f;
    public float angleRotation = 25.0f;
    private bool rightRotation;
    private bool leftRotation;
    private Vector3 posSpawnMonster= new Vector3(0, 0, 0);

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(speedTranslate, 0.0f);
        leftRotation = false;
        rightRotation = true;
        posSpawnMonster = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // D�placement de Monster Fly, fait des allers-retours sur x
        if (CompareTag("MonsterFly"))
        {
            if (transform.position.x >= 2.4f)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-speedTranslate, 0.0f);
            }
            else if (transform.position.x <= -2.4f)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(speedTranslate, 0.0f);
            }

            if (leftRotation)
            {
                transform.Rotate(0.0f, 0.0f, speedRotation);
                if (transform.rotation.eulerAngles.z >= angleRotation && transform.rotation.eulerAngles.z <= 360 - angleRotation)
                {
                    leftRotation = false;
                    rightRotation = true;
                }
            }
            else if (rightRotation)
            {
                transform.Rotate(0.0f, 0.0f, -speedRotation);
                if (transform.rotation.eulerAngles.z <= 360 - angleRotation && transform.rotation.eulerAngles.z >= angleRotation)
                {
                    rightRotation = false;
                    leftRotation = true;
                }
            }
        }
        // "D�placement" de Monster fixe
        if (CompareTag("MonsterFixe"))
        {
            if (leftRotation)
            {
                transform.Rotate(0.0f, 0.0f, speedRotation);
                if (transform.rotation.eulerAngles.z >= angleRotation && transform.rotation.eulerAngles.z <= 360 - angleRotation)
                {
                    leftRotation = false;
                    rightRotation = true;
                }
            }
            else if (rightRotation)
            {
                transform.Rotate(0.0f, 0.0f, -speedRotation);
                if (transform.rotation.eulerAngles.z <= 360 - angleRotation && transform.rotation.eulerAngles.z >= angleRotation)
                {
                    rightRotation = false;
                    leftRotation = true;
                }
            }
        }
        // D�placement de MonsterFall, monte et tombe
        if (CompareTag("MonsterFall"))
        {
            // Si le monstre est sur �a pause de d�part il tombe
            if(transform.position.y >= posSpawnMonster.y)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            }
            // Si il est tomb� sur plus de 1.5y de distance, il s'arr�te et remonte
            if(transform.position.y + 1.5 < posSpawnMonster.y)
            {
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1.0f);
            }
        }
    }
}
