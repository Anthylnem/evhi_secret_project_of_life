using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement du prefab Bullet
public class Bullet : MonoBehaviour
{
    public float speed = 800.0f;
    public Vector2 targetVector = new Vector2(0,1);
    public AudioClip monsterShootClip;
    public int bonusKillMonster = 50;

    private AudioSource bulletSource;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
        rb.AddForce(targetVector * speed);

        bulletSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //D�tecte si bullet entre en collision avec un monstre et d�truit le GO
        if (collision.gameObject.CompareTag("MonsterFly") || collision.gameObject.CompareTag("MonsterFall") || collision.gameObject.CompareTag("MonsterFixe"))
        {
            bulletSource.PlayOneShot(monsterShootClip, 1.0f);
            collision.gameObject.SetActive(false);
            Destroy(gameObject);

            // Ajout bonus kill quand monstre tu�
            GameObject.Find("Main Camera").GetComponent<CameraUp>().score += bonusKillMonster;
        }
    }
}
