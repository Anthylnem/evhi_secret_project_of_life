using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement des plateformes vertes et bleues, et des bonus
public class Jump : MonoBehaviour
{
    public float jumpForce = 14.0f;
    public AudioClip JumpSound;
    public Sprite springShoesUp;

    private AudioSource JumpAudio;

    // Start is called before the first frame update
    void Start()
    {
        JumpAudio = GetComponent<AudioSource>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && collision.GetComponent<Rigidbody2D>().velocity.y < 0 && !CompareTag("BluePF") && !CompareTag("GreenPF") && !collision.GetComponent<ControllerPlayer>().jumpUp)
        {
            jumpMode(collision);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //D�tecte si doodle entre en collision avec la plateforme et dans ce cas le repropulse
        if (collision.gameObject.CompareTag("Player") && collision.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            jumpMode(collision);
        }
    }

    void jumpMode(Collider2D collision)
    {
        if ((CompareTag("GreenPF") || CompareTag("BluePF")) && transform.position.y + 0.3f < collision.transform.position.y)
        {
            Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
            Vector2 velocity = new Vector2(0.0f, jumpForce);
            rb.velocity = velocity;
            if (collision.gameObject.GetComponent<ControllerPlayer>().bonusJump == true)
                collision.gameObject.GetComponent<ControllerPlayer>().bonusJump = false;
            JumpAudio.PlayOneShot(JumpSound, 1.0f);
        }
        else if (!CompareTag("GreenPF") && !CompareTag("BluePF"))
        {
            Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
            Vector2 velocity = new Vector2(0.0f, jumpForce);
            rb.velocity = velocity;
            if (collision.gameObject.GetComponent<ControllerPlayer>().bonusJump == true)
                collision.gameObject.GetComponent<ControllerPlayer>().bonusJump = false;
        }

        // Active les bonus
        if (gameObject.CompareTag("Spring"))
        {
            GetComponent<SpriteRenderer>().sprite = springShoesUp;
            GameObject.Find("Game Manager").GetComponent<GameManager>().startSpringShoesSong();
            collision.gameObject.GetComponent<ControllerPlayer>().bonusJump = true;
            collision.gameObject.GetComponent<ControllerPlayer>().startSpring();
        }
        if (gameObject.CompareTag("Hat"))
        {
            GameObject.Find("Game Manager").GetComponent<GameManager>().startHatSong();
            collision.gameObject.GetComponent<ControllerPlayer>().bonusJump = true;
            collision.gameObject.GetComponent<ControllerPlayer>().startHat();
        }
        if (gameObject.CompareTag("Jetpack"))
        {
            GameObject.Find("Game Manager").GetComponent<GameManager>().startJetPackSong();
            collision.gameObject.GetComponent<ControllerPlayer>().bonusJump = true;
            collision.gameObject.GetComponent<ControllerPlayer>().startJetPack();
        }
    }
}
