using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Comportement des pi�ges : Monster et Hole
public class CollisionTrap : MonoBehaviour
{
    public AudioClip trapSound;
    public AudioClip killSound;
    public bool playerLeftRotation;
    public bool playerRightRotation;
    public float playerSpeedRotation = 1.0f;
    public float playerAngleRotation = 10.0f;
    public float jumpForce = 14.0f;
    public int bonusKillMonster = 50;

    private AudioSource trapAudio;
    private bool playerInTrap;
    private Rigidbody2D playerRB;

    // Start is called before the first frame update
    void Start()
    {
        trapAudio = GetComponent<AudioSource>();
        playerInTrap = false;
        playerLeftRotation = false;
        playerRightRotation = false;
        playerRB = null;

}

// Update is called once per frame
void Update()
    {
        if (playerInTrap)
        {
            ShockPlayer();
        }
    }

    //Si le joueur entre en collision avec le pi�ge, il s'arr�te et se secoue sur le monstre
    //fin du jeu
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && collision.gameObject.GetComponent<ControllerPlayer>().bonusJump == false)
        {
            if (collision.GetComponent<Rigidbody2D>().velocity.y > 0)
            {
                playerRB = collision.GetComponent<Rigidbody2D>();
                playerRB.transform.position = new Vector3(transform.position.x, transform.position.y, playerRB.transform.position.z);
                Vector2 velocity = new Vector2(0.0f, 0.0f);
                playerRB.velocity = velocity;
                trapAudio.PlayOneShot(trapSound, 1.0f);
                playerRB.bodyType = RigidbodyType2D.Kinematic;

                GameObject.Find("Game Manager").GetComponent<GameManager>().gameOver();

                playerInTrap = true;
                playerRightRotation = true;
            }
            else
            {
                Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
                Vector2 velocity = new Vector2(0.0f, jumpForce);
                rb.velocity = velocity;
                trapAudio.PlayOneShot(killSound, 1.0f);
                GetComponent<BoxCollider2D>().isTrigger = false;
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                GameObject.Find("Main Camera").GetComponent<CameraUp>().score += bonusKillMonster; 
            }
        }
    }

    // Animation quand le Doodle est attrap�
    void ShockPlayer()
    {
        playerRB.freezeRotation = false;
        playerRB.transform.position = new Vector3(transform.position.x, transform.position.y, playerRB.transform.position.z);

        if (playerLeftRotation)
        {
            playerRB.transform.Rotate(0.0f, 0.0f, playerSpeedRotation);
            if (playerRB.transform.rotation.eulerAngles.z >= playerAngleRotation && playerRB.transform.rotation.eulerAngles.z <= 360 - playerAngleRotation)
            {
                playerLeftRotation = false;
                playerRightRotation = true;
            }
        }
        else if (playerRightRotation)
        {
            playerRB.transform.Rotate(0.0f, 0.0f, -playerSpeedRotation);
            if (playerRB.transform.rotation.eulerAngles.z <= 360 - playerAngleRotation && playerRB.transform.rotation.eulerAngles.z >= playerAngleRotation)
            {
                playerRightRotation = false;
                playerLeftRotation = true;
            }
        }
    }
}
