# EVHI_Doodle_jump_unity

Ce projet est un projet de M2 [ANDROIDE](http://androide.lip6.fr/) ([Sorbonne Université](https://www.sorbonne-universite.fr/)) de l'UE EVHI (Environnement Hautement Interactif).

Le but était de determiner le niveau d'utilisation de Unity de la part des étudiants après 2 semaines de tutoriel. Nous devions reproduire une version du jeu Doodle Jump dans une moindre mesure. Le cahier des charges se trouve au niveau du fichier racine sous l'intitulé "Cahier des charges".

Le projet suivant a été réalisé en binôme : Mme Richaume Anthéa et M. Maillos Sébastien
